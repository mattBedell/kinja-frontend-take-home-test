import React from 'react';
import styled from 'styled-components';

const Select = styled.select`
`;

const Option = styled.option`
`;

export const Dropdown = ({ placeholder, options, name, handleChange }) => {
    return (
      <Select name={name} onChange={handleChange}>
        <Option value="" defaultValue>{placeholder}</Option>
        {options.map((opt) => <Option value={opt.value} key={`option-${opt.name}-${opt.value}`}>{opt.name}</Option>)}
      </Select>
    )
}
