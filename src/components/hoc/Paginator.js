import React, { useState, useEffect, useMemo } from 'react'


export const withPaginator = (Wrapped) => {
  return ({ items, pageSize }) => {
    const [page, setPage] = useState(0);
    // batch items based on page index and page size
    const itemBatch = useMemo(() => {
      const batchStart = page * pageSize;
      return items.slice(batchStart, batchStart + pageSize);
    }, [items, page, pageSize]);

    // determine if pagination buttons should be enabled based on page and items
    const nextButtonEnabled = (page * pageSize <= items.length - pageSize);
    const prevButtonEnabled = page > 0;

    // handle pagination nav and change page index accordingly
    const handlePage = (pageNum) => {
      let newPage = page + pageNum;
      if (newPage < 0) {
        newPage = 0;
      } else if (newPage * pageSize > items.length) {
        newPage = page;
      }
      setPage(newPage);
    }

    // reset page index when items get filtered or sorted
    // @TODO - useEffect is probably not the right tool for this
    useEffect(() => {
      setPage(0);
    }, [items]);

    return (
      <>
        <Wrapped users={itemBatch} />
        <div>
          <button disabled={!prevButtonEnabled} onClick={() => handlePage(-1)}>Prev</button>
          <button disabled={!nextButtonEnabled} onClick={() => handlePage(1)}>Next</button>
        </div>
      </>
    )
  }
}
