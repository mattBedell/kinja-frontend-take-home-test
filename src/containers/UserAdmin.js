// @flow

import React, { useState, useEffect, useMemo } from 'react'
import styled from 'styled-components'

import logo from '../assets/keki.png'

import { getUsers } from '../api/profile'

import { UserList } from '../components/UserList'

import { Dropdown } from '../components/patterns/Dropdown';

import { withPaginator } from '../components/hoc/Paginator';

const PaginatedUserList = withPaginator(UserList);

const Loading = () => <p>Loading</p>


const UserListContainer = styled.section`
    max-width: 1024px;
    margin: 0 auto;
    padding: 0 30px;
`

const ListHeader = styled.header`
    color: #222;
    padding: 30px 15px 15px;
    h2 {
        margin: 0;
    }
`

const AdminHeader = styled.header`
    margin: 0;
    height: 67px;
    padding: 10px;
    background: url('${logo}') left 10px center no-repeat #222;
`

const ListFooter = styled(ListHeader)`
    text-align: center;
`

export const UserAdmin = (props) => {
    const [users, setUsers] = useState([]);
    const [filters, setFilters] = useState({});
    const [sorters, setSorters] = useState({});
    const [isLoading, setIsLoading] = useState(true);
    // @TODO - make this network request cancelable so state is not updated on dismounted component
    useEffect(() => {
        getUsers()
            .then((fetchedUsers) => {
                setUsers(fetchedUsers);
                return fetchedUsers;
            })
            .then(() => setIsLoading(false));
    }, []);

    const roleOptions = useMemo(() => {
        // get all available roles from fetched users and construct options for their filter form
        return Array.from(users.reduce((roleAccuSet, { role }) => {
            roleAccuSet.add(role);
            return roleAccuSet;
        }, new Set())).map((role) => ({ name: role.toUpperCase(), value: role }));
    }, [users]);

    // filter users based on filter selections
    const filteredUsers = useMemo(() => {
        return Object.keys(filters).reduce((filtered, filterKey) => {
            return filtered.filter((user) => {
                const filterValue = filters[filterKey];
                // ignore falsey filter values that are not useful to filter on
                if (filterValue === undefined || filterValue === null || isNaN(filterValue)) {
                    return true;
                }

                return user[filterKey] === filterValue;
            });
        }, users);
    }, [users, filters]);

    const sortedUsers = useMemo(() => {
        const userCopy = [...filteredUsers];
        const sortDirection = sorters.name;

        // no sort selected, return original order
        if (!sortDirection) {
            return userCopy;
        }

        // get last name and downcase for sorting users
        const getLastName = (name) => {
            const names = name.split(' ');
            return names[names.length - 1].toLowerCase();
        }

        // toggle sort direction based on user selection
        // @TODO remove hardcoded sort values
        const sortModifier = sortDirection === 'ASC' ? 1 : -1;
        userCopy.sort(({ name: aName }, { name: bName }) => {
            let compare = 0;

            const lastA = getLastName(aName);
            const lastB = getLastName(bName);

            if (lastA < lastB) {
                compare = -1;
            } else if (lastA > lastB) {
                compare = 1;
            }

            return compare * sortModifier;
        });

        return userCopy;
    }, [filteredUsers, sorters]);

    // update selected filters
    const handleFilter = (e) => {
        let { name, value } = e.target;

        // @TODO - gross, clean that up
        if (value === 'true') {
            value = true
        } else if (value === 'false') {
            value = false;
        } else if (value === '') {
            value = undefined;
        }

        setFilters((state) => ({
            ...state,
            [name]: value,
        }));
    }

    // update selected sort, hardcoded to the name field for now
    // @TODO this could be better
    const handleSort = (e) => {
        let { value } = e.target;
        setSorters({ 'name': value });
    }

    return (
        <>
            <AdminHeader />
            <UserListContainer>
                <ListHeader>
                    <h2>Users</h2>
                </ListHeader>
                <Dropdown placeholder="Filter By Role" name="role" options={roleOptions} handleChange={handleFilter} />
                <Dropdown placeholder="Filter By Verified" name="verified" options={[{ value: true, name: 'Verified'}, { value: false, name: 'Not Verified'}]} handleChange={handleFilter}/>
                <Dropdown placeholder="Sort By Name" name="sort" options={[{ value: 'ASC', name: 'Ascending'}, { value: 'DESC', name: 'Descending'}]} handleChange={handleSort}/>
                {isLoading
                    ? <Loading />
                    : <PaginatedUserList items={sortedUsers} pageSize={5} />
                }
                <ListFooter>
                </ListFooter>
            </UserListContainer>
        </>
    )
}
